<?php

namespace Drupal\poke_show_content\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the poke_show_content module.
 */
class DefaultControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "poke_show_content DefaultController's controller functionality",
      'description' => 'Test Unit for module poke_show_content and controller DefaultController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests poke_show_content functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module poke_show_content.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
