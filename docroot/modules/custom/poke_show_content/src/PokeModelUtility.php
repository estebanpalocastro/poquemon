<?php

namespace Drupal\poke_show_content;

use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class VersionModelUtility.
 */
class PokeModelUtility {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Database\database definition.
   *
   * @var \Drupal\Core\Database\database
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Create new favorite.
   */
  public function createFavorite($uid, $id_poke) {
    $date_time = new \DateTime();
    $timestamp = $date_time->getTimestamp();
    $fields = [
      'uid_user' => $uid,
      'id_poke' => $id_poke,
      'favorite_timestamp' => $timestamp,
    ];

    $query = $this->database->insert('poke_favorite');
    $query->fields($fields);
    return $query->execute();
  }

  /**
   * Delete favorite.
   */
  public function deleteFavorite($uid, $id_poke) {
    return $this->database->delete('poke_favorite')
      ->condition('uid_user', $uid)
      ->condition('id_poke', $id_poke)
      ->execute();
  }

  /**
   * Get list of favorites by user.
   */
  public function getFavoritesByUser($uid) {
    $query = $this->database->select('poke_favorite', 'fp');
    $query->fields('fp', ['id_poke']);
    $query->condition('uid_user', $uid);
    return $query->execute()->fetchCol();

  }
}
