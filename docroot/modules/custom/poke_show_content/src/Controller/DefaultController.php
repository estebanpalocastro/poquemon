<?php

namespace Drupal\poke_show_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\poke_show_content\PokeModelUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\poke_show_content\PokeModelUtilityt;
use Drupal\Core\Session\AccountProxy;
/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\poke_show_content\PokeApiHttpClient definition.
   *
   * @var \Drupal\poke_show_content\PokeModelUtility
   */
  protected $apiPoke;

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    PokeModelUtility $poke_api_model,
    AccountProxy $account
  ) {
    $this->apiPoke = $poke_api_model;
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('poke_api.utility'),
      $container->get('current_user')
    );
  }

  /**
   * Home.
   *
   * @return array
   *   Return view home render.
   */
  public function home() {
    $icon_compare = \Drupal::theme()->getActiveTheme()->getPath() . '/assets/compare.svg';
    $uid = $this->currentUser->id();
    $favorites = $this->apiPoke->getFavoritesByUser($uid);
    return [
      '#theme' => 'poke_show_content',
      '#attached' => [
        'drupalSettings' => [
          'path_icon_compare' => $icon_compare,
          'favorites' => $favorites,
        ],
      ]
    ];
  }
  /**
   * Favorites.
   *
   * @return array
   *   Return favorites render.
   */
  public function favorites() {
    $icon_compare = \Drupal::theme()->getActiveTheme()->getPath() . '/assets/compare.svg';
    $uid = $this->currentUser->id();
    $favorites = $this->apiPoke->getFavoritesByUser($uid);
    return [
      '#theme' => 'poke_show_content_favorites',
      '#attached' => [
        'drupalSettings' => [
          'path_icon_compare' => $icon_compare,
          'favorites' => $favorites,
          'is_favorite_page' => TRUE,
        ],
      ]
    ];
  }

}
