<?php

namespace Drupal\poke_show_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\poke_show_content\PokeModelUtility;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Session\AccountProxy;
/**
 * Class DefaultController.
 */
class ApiPokeController extends ControllerBase {

  /**
   * Drupal\poke_show_content\PokeApiHttpClient definition.
   *
   * @var \Drupal\poke_show_content\PokeApiHttpClient
   */
  protected $apiPoke;

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    PokeModelUtility $poke_model,
    AccountProxy $account
  ) {
    $this->apiPoke = $poke_model;
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('poke_api.utility'),
      $container->get('current_user')
    );
  }

  /**
   * Home.
   *
   * @return string
   *   Return Hello string.
   */
  public function getFavoritesByUser(Request $request) {
    $uid = $this->currentUser->id();
    $favorites = $this->apiPoke->getFavoritesByUser($uid);
    $response['data'] = $favorites;
    return new JsonResponse($response, 200);
  }

  /**
   * Favorites.
   *
   * @return string
   *   Return Hello string.
   */
  public function createFavorite(Request $request) {
    if (isset($request->query)) {
      $post = $request->request->all();
      $uid = $this->currentUser->id();

      $favorites = $this->apiPoke->getFavoritesByUser($uid);
      if (!in_array($post['id_poke'], $favorites)) {
        $result = $this->apiPoke->createFavorite($uid, $post['id_poke']);
        return new JsonResponse(['statusText' => 'success'], 200);
      }
      return new JsonResponse(['statusText' => 'repeat'], 200);
    }
    return new JsonResponse(['statusText' => 'fail', 400]);
  }

  /**
   * Favorites.
   *
   * @return string
   *   Return Hello string.
   */
  public function deleteFavorite(Request $request) {
    if (isset($request->query)) {
      $post = $request->request->all();
      $uid = $this->currentUser->id();
      $result = $this->apiPoke->deleteFavorite($uid, $post['id_poke']);
      if ($result) {
        return new JsonResponse(['statusText' => 'success'], 200);
      }
    }
    return new JsonResponse(['statusText' => 'fail', 400]);
  }

}
