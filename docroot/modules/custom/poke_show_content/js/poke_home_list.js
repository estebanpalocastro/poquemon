const checkItems = function (id) {
  const itemsToCompare = jQuery('input.custom-control-input:checked').length;
  if (itemsToCompare >= 2) {
    jQuery('#pokeDetail').modal('show');
  }
};

(function ($, Drupal) {
  'use strict';

  /**
   * Behavior to Model Mortgage rnc block.
   */
  Drupal.behaviors.homeShowItems = {
    attach: function (context, settings) {
      const pokedex = document.getElementById('pokemon-list');
      const favorites = drupalSettings.favorites;
      const state = {
        pokemons: null,
        selectedPokemons: null,
      };

      const displayPokemon = (input = '') => {
        $('#pokemon-list').empty();
        if (drupalSettings.is_favorite_page && favorites.length > 0) {
          $('.text-favorites').remove();
        }
        const pokemonFilter = (drupalSettings.is_favorite_page) ? state.pokemons : filterByName(state.pokemons, input);
        const pokemonHTMLString = pokemonFilter.map((pokeman) => {
          if (pokeman) {
            return `
              <div class="poke-container col-lg-3 col-md-6 col-sm-12">
              <div class="poke-card text-center ${(favorites.includes(pokeman.id.toString())) ? 'favorite' : ''}" id="pokemon-${pokeman.id}">
                  <a href="#pokeDetail" data-toggle="modal" data-ruta="${pokeman.id}" title="add to favorites" data-id="${pokeman.id}" data-name="${pokeman.name}" data-target="#pokeDetail">
                    <img class="card-image img-responsive" src="${pokeman.image}"/>
                  </a>
                  <div class="extra-info text-center">
                    <div class="custom-control custom-checkbox">
                    <a href="" class="like" data-toggle="tooltip" data-placement="top" title="Like" data-id="${pokeman.id}"></a>
                    <input type="checkbox" data-id="${pokeman.id}" class="custom-control-input" id="customCheck-${pokeman.id}" onchange="checkItems(${pokeman.id})">
                    <label class="custom-control-label" for="customCheck-${pokeman.id}">` + Drupal.t('Compare') + `</label>
                    <h2 class="card-title text-center">${pokeman.name}</h2>
                  </div>
                  </div>
                  </a>
              </div>
              </div>`;
          }
        }
        ).join('');
        pokedex.innerHTML = pokemonHTMLString;
      };

      const fetchPokemon = () => {
        $.ajax({
          url: 'https://pokeapi.co/api/v2/pokemon?limit=150',
          dataType: 'json',
          async: false,
          beforeSend: function () {
            $('#image-loader').show();
          },
          complete: function () {
            $('#image-loader').hide();
          },
          success: function (data) {
            if (data['results']) {
              state.pokemons = data.results.map((data, index) => {
                  const id = index + 1;
                  if (drupalSettings.is_favorite_page) {
                    var isFavorite = favorites.includes(id.toString());
                    if (isFavorite) {
                      return {
                        name: data.name,
                        id: index + 1,
                        image: `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/` + ('000' + (index + 1)).slice(-3) + `.png`
                      };

                    }
                  }
                  else {
                    return {
                      name: data.name,
                      id: index + 1,
                      image: `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/` + ('000' + (index + 1)).slice(-3) + `.png`
                    };
                  }
                }
              );
            }
          }
        });
      }

      $('.btn-close-modal').on('click', function (event) {
        $('input.custom-control-input').prop('checked', false);
      });

      function filterByName(pokemons,query) {
        var filter = pokemons.filter((pokemon)=>{
          return pokemon.name.toLowerCase().startsWith(query.toLowerCase())==true;
        });
        state.selectedPokemons = filter;
        return filter;
      }

      $('.btn-add-favorite').on('click', function (event) {
        var id = $(this).data('id');
        var params = {'id_poke' :id};
        var data = processAjax('/api/v1/add_favorite', params);
        if (data) {
          $('#pokeDetail').modal('hide');
          favorites.push(id.toString());
          $('#pokemon-' + id).addClass('favorite');
        }
      });

      $('.btn-remove-favorite').on('click', function (event) {
        var id = $(this).data('id');
        var params = {'id_poke' :id};
        var data = processAjax('/api/v1/remove_favorite', params);
        if (data) {
          $('#pokeDetail').modal('hide');
          favorites.splice(favorites.indexOf(id.toString()), 1);
          $('#pokemon-' + id).removeClass('favorite');
          if (drupalSettings.is_favorite_page) {
            $('#pokemon-' + id).remove();
          }
        }
      });



      $('#pokeDetail').on('show.bs.modal', function (event) {
        var modal = $(this);
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('ruta') // Extract info from data-* attributes
        var id = button.data('id');
        var nombre = button.data('name');
        var itemsToCompare = $('input.custom-control-input:checked');
        if (button.length > 0) {
          $('#pokeDetail .modal-dialog').removeClass('modal-lg');
          $('#pokeDetail .modal-dialog').addClass('modal-md');
          var isFavorite = favorites.includes(id.toString());
          $('.poke-card-detail-compare').addClass('hide');
          if (isFavorite) {
            $('.btn-remove-favorite').removeClass('hide');
            $('.btn-add-favorite').addClass('hide');
          }
          else {
            $('.btn-add-favorite').removeClass('hide');
            $('.btn-remove-favorite').addClass('hide');
          }

          if (favorites.length > 9 || isFavorite || drupalSettings.is_favorite_page) {
            $('.btn-add-favorite').addClass('hide');
          }


          $('.poke-card-detail').removeClass('col-md-6');
          $('.btn-add-favorite').data('id', id);
          $('.btn-remove-favorite').data('id', id);
          generatePokemonDetail('.poke-card-detail', id);
        }
        else {
          $('#pokeDetail .modal-dialog').addClass('modal-lg');
          $('#pokeDetail .modal-dialog').removeClass('modal-md');
          $('.poke-card-detail-compare').removeClass('hide');
          $('.btn-primary').addClass('hide');
          $.each(itemsToCompare , function(index, pokemon) {
            var idPoke = $(pokemon).data('id');
            if (index > 0) {
              generatePokemonDetail('.poke-card-detail-compare', idPoke);
            }
            else {
              $('.poke-card-detail').addClass('col-md-6');
              generatePokemonDetail('.poke-card-detail', idPoke);
            }
          });
        }
      });

      function processAjax(url, params = []) {
        var response = false;
        $.ajax({
          url: url,
          type: 'POST',
          async: false,
          data: params,
          dataType: 'json',
          fail: function (data) {
            response = false;
          },
          success: function (data) {
            response = true;
          }
        });
        return response;
      }

      function generatePokemonDetail(parentContainer, id) {
        $.ajax({
          url: 'https://pokeapi.co/api/v2/pokemon/' + id,
          dataType: 'json',
          success: function (data) {
            if (data) {
              var image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/` + ('000' + id).slice(-3) + `.png`;
              $(parentContainer +' .modal-image img').attr('src', image);
              $(parentContainer +' .modal-name span').text(data.name);
              $(parentContainer +' .poke-name').text(data.name);
              $(parentContainer +' .modal-height span').text(data.height);
              $(parentContainer +' .modal-weight span').text(data.weight);
              $(parentContainer +' .modal-ability li').remove();
              data.abilities.forEach(function (element) {
                $(parentContainer +' .modal-ability').append('<li>' + element.ability.name + '</li>');
              });
              var tipoUrl = [];
              $(parentContainer +' .modal-type li').remove();
              data.types.forEach(function (element) {
                const pokeType = $('<li class="type">' + element.type.name + '</li>');
                $(parentContainer +' .modal-type').append(pokeType);
                tipoUrl.push(element.type.url);
              });

              tipoUrl.forEach(function (link) {
                $.get(link, function (response) {
                  $(parentContainer +' .modal-weakness li').remove();
                  response.damage_relations.double_damage_from.forEach(function (element) {
                    const weakness = $('<li>' + element.name + '</li>');
                    $(parentContainer +' .modal-weakness').append(weakness);
                  });
                });
              });
            }
          }
        });

        $.ajax({
          url: 'https://pokeapi.co/api/v2/pokemon-species/' + id,
          dataType: 'json',
          success: function (data) {
            if (data) {
              $(parentContainer +' .modal-description').text('');
              $(parentContainer +' .modal-description').text(data.flavor_text_entries[1].flavor_text);
              $(parentContainer +' .modal-category li').remove();
              data.genera.forEach(function(element) {
                if (element.language.name == "en") {
                  $(parentContainer +' .modal-category').append('<li>' + element.genus + '</li>');}
              });
            }
          }
        });
      }

      $("#poke-search").on("keyup", (e) => {
        e.preventDefault();
        var searchText = $('#poke-search').val();
        displayPokemon(searchText);
      });
      fetchPokemon();
      displayPokemon();
    }
  };
})(jQuery, Drupal);
