var gulp          = require('gulp');
var sass          = require('gulp-sass');
var plumber       = require('gulp-plumber');
var autoprefixer  = require('gulp-autoprefixer');
var minifyCSS     = require('gulp-minify-css');
var watch         = require('gulp-watch');

gulp.task('watch',['sass'], function() {
  gulp.watch('./sass/**/*.scss', ['sass']);
})

gulp.task('sass', function() {
  gulp.src('./sass/**/*.scss')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log('Message:', error.message);
        console.log('   File:', error.fileName);
        console.log('   Line:', error.lineNumber);
        this.emit('end');
      }}))

    .pipe(sass({
      style: 'compact'
    }))

    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))

    .pipe(gulp.dest('./css'));
});
